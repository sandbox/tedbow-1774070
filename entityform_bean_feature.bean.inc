<?php
/**
 * @file
 * entityform_bean_feature.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function entityform_bean_feature_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'entityform_type_block';
  $bean_type->label = 'Entityform Type Block';
  $bean_type->options = '';
  $bean_type->description = 'Embeds an Entityform type in a Block';
  $export['entityform_type_block'] = $bean_type;

  return $export;
}
