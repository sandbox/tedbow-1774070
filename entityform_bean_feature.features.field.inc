<?php
/**
 * @file
 * entityform_bean_feature.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function entityform_bean_feature_field_default_fields() {
  $fields = array();

  // Exported field: 'bean-entityform_type_block-field_entityform_type'.
  $fields['bean-entityform_type_block-field_entityform_type'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_entityform_type',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(
        'node' => array(
          'columns' => array(
            'target_id' => 'nid',
          ),
          'table' => 'node',
        ),
      ),
      'indexes' => array(
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => '0',
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'direction' => 'ASC',
            'field' => 'none',
            'property' => 'label',
            'type' => 'property',
          ),
          'target_bundles' => array(),
        ),
        'handler_submit' => 'Change handler',
        'target_type' => 'entityform_type',
      ),
      'translatable' => '0',
      'type' => 'entityreference',
    ),
    'field_instance' => array(
      'bundle' => 'entityform_type_block',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Select the Entityform Type to embed',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'view_mode' => 'full',
          ),
          'type' => 'entityreference_entity_view',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'bean',
      'field_name' => 'field_entityform_type',
      'label' => 'Entityform Type',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Entityform Type');
  t('Select the Entityform Type to embed');

  return $fields;
}
