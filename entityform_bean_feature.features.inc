<?php
/**
 * @file
 * entityform_bean_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function entityform_bean_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
}
